TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        combo.cpp \
        inventory.cpp \
        main.cpp

HEADERS += \
    combo.h \
    inventory.h

DISTFILES += \
    URLVIDEO.txt \
    combos.txt \
    inventory.txt \
    market.txt \
    report.txt \
    sudo \
    users
