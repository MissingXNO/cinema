#ifndef INVENTORY_H
#define INVENTORY_H
#include <string>
#include <map>
#include <vector>

using namespace std;

class inventory
{
private:
    struct product
    {
        string name;
        int defquantity;
        int quantity;
        float cost;
        int neto;
        float unitcost;
    }p;
    map<string,product>inv;

public:
    inventory();
    void add_to_inventory();
    void show_inventory();
    void write_on_file();
    void load_from_file();
    int extract_quantity(string ID, int quantity);
    int product_exist(string ID);
};

#endif // INVENTORY_H
