#ifndef COMBO_H
#define COMBO_H

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <inventory.h>

using namespace std;

class combo
{
private:
    struct com
    {
        string name;
        float price;
        vector<string>IDS;
        vector<int>Q;
    }c;
    list<com>combolist;
public:
    combo();
    void add_default_combos();
    void create_combo(string name, vector<string> ids, vector<int> qs, float price);
    void show_combos();
    void write_on_file();
    void load_from_file();
    void show_inventory();
    int combo_exist(int ID);
    int transaction(int opt, inventory &inv, float payment);
    void return_change(float price, float payment);
};

#endif // COMBO_H
