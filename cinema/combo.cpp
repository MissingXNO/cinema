#include "combo.h"
#include "inventory.h"
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

combo::combo()
{

}

void combo::add_default_combos()
{
    //Se añaden 3 combos por defecto en el inventario. Esta opción solo se una en caso de pérdida del archivo combos.txt y
    //no permite la ejecución del programa. Solo lo hace para ingresar los combos por defecto en caso de haber fallos.
    //Función creada para tareas de depuración

    // Se introducen parámetros a la estructura c de la clase combo desde el código.

    combo::c.name="2 Perros sencillos + 2 gaseosas medianas";
    combo::c.price=35000;
    combo::c.IDS={"7","18","5","1","12","13"};
    combo::c.Q={2,2,2,2,2,2};
    combo::combolist.push_back(combo::c);   //se agregan estructuras c una detrás de la otra por medio de push_back()

    combo::c.name="2 hamburguesas sencillas + 2 gaseosas grandes + 1 crispetas mix grandes";
    combo::c.price=40000;
    combo::c.IDS={"3","19","5","1","12","13","24","25"};
    combo::c.Q={2,2,2,2,2,2,1,1};
    combo::combolist.push_back(combo::c);

    combo::c.name="2 ordenes de nachos + 2 gaseosas + 2 chocolatinas medianas";
    combo::c.price=37000;
    combo::c.IDS={"20","1","21","26","23","5"};
    combo::c.Q={2,2,2,6,6,4};
    combo::combolist.push_back(combo::c);
    //write_on_file();
}

void combo::create_combo(string name, vector<string> ids, vector<int> qs, float price)
{
    /*Ingresa los parámetros requeridos a la estructura c de la clase combo y recibe dichos parametros por copia desde el main*/
    combo::c.name=name;
    combo::c.IDS=ids;
    combo::c.Q=qs;
    combo::c.price=price;
    combo::combolist.push_back(combo::c);
    cout<<"\nSE AGREGO EL COMBO "<<name;
    write_on_file(); //Reescribe los combos en el archivo combos.txt
}

void combo::show_combos()
{
    //recorre la lista actual (la que está almacenada en la lista combolist de la clase combo durante tiempo de ejecución) y muestra los combos en el fomarto establecido
    list<combo::com>::iterator it;
    int i=1; //i indica el numero del combo. Es un identificador para que el cliente pueda elegir.
    cout<<"\nMENU DE COMBOS\n------------------------------------------------------------------------------------------------------------------------\n";
    for(it=combo::combolist.begin();it!=combo::combolist.end();it++)
    {
        cout<<"COMBO   | "<<i<<endl;
        cout<<"PRODUCTO| "<<it->name<<endl;
        cout<<"PRECIO  | "<<it->price<<endl;
        cout<<"------------------------------------------------------------------------------------------------------------------------"<<endl;
        i++;
    }
}

void combo::write_on_file()
{
    /*Reescribe en el archivo combos.txt todas los combos que estén en la lista combolist de la clase combos durante el tiempo de ejecución.*/
    ofstream combos("combos.txt");  //Accede al archivo combos.txt para escritura
    list<com>::iterator it; //iterador sobre una lista que almacena estructuras com de la clase combos. com almacena las estructuras de los combos, con sus características
    //para extraer la información de forma ordenada, el programa se apoya de que las lineas del archivo fueron escritas con separadores (caracter '/')
    for(it=combolist.begin();it!=combolist.end();it++)
    {
        combos<<it->name<<"/"<<it->price<<"/";
        vector<string>::iterator it2; //iterador para recorrer el vector de strings que contiene los id de los ingredientes que componen al combo
        for(it2=it->IDS.begin();it2!=it->IDS.end();it2++)
        {
            combos<<*it2<<",";
        }
        combos<<"/";
        vector<int>::iterator it3;                  // iterador para recorrer el vectore de enteros que contiene las cantidades de dichos ingredientes
                                                    /*(considerese que los ids y las cantidades se corresponden según sus posiciones en los vectores,
                                                     por ejemplo, el ingrediente con id 23 está almacena en la posición 0 del vector ids, y la
                                                     cantidad de dicho ingrediente está almacenada en la posición 0 del vector qs)*/
        for(it3=it->Q.begin();it3!=it->Q.end();it3++)
        {
            combos<<*it3<<",";
        }
        combos<<"\n";
    }
    combos.close(); //cerramos el objeto archivo 'combos'
}

void combo::load_from_file()
{
    /*Archivo para cargar los combos desde el archivo combos.txt. Funciona de manera similar al load_from_file() de la clase inventory*/
    ifstream combos("combos.txt"); //Se abre el archivo combos.txt
    string line;
    while(!combos.eof())    //se recorre el archivo linea por linea
    {
        string name="";
        string price="";
        vector<string>IDS;
        vector<int>Q;
        string num="";
        getline(combos,line); //se almacena la linea actual en line
        if(line[0]=='\0'||line[0]=='\n')
            break; //si la linea empieza con el caracter nulo, cierre la carga (se espera una linea vacía)
        int i=0;
        while(line[i]!='/')
        {
            name+=(line[i]);    //se almacena el nombre del combo
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            price+=(line[i]);       //luego su precio
            i++;
        }
        float nprice=stoi(price,nullptr,10); //el cual se convierte a int y se le hace casting a float
        i++;
        while(line[i]!='/')
        {
            num="";
            while(line[i]!=',') //se captan uno por uno los id
            {
                num+=line[i];   //se almacena el id actual en num
                i++;
            }
            IDS.push_back(num); //ese id se almacena en el vector de strings IDS
            i++;
        }
        i++;
        while(line[i]!='\0')    //se captan una por una las cantidades
        {
            num="";
            while(line[i]!=',')
            {
                num+=line[i];       //se almacena la cantidad actual en num
                i++;
            }
            int n=stoi(num,nullptr,10);     //se le convierte a entero
            Q.push_back(n);         //se la ingresa en el vector de enteros Q
            i++;
        }
        i++;
        line.clear();   //se limpia el string line
        this->c={name,nprice,IDS,Q}; //se almacenan las características captadas a lo largo de la función en la estructa c de esta clase
        combolist.push_back(c); //Esta estructura se almacena en la lista combolist de este clase
    }
    combos.close();
}

void combo::show_inventory()
{
    /*Se muestra el inventario almacenado en el archivo invetory.txt (NO el que esté en la clase inventory, dado que es parametro privado de la misma)*/
    //su funcionamiento es casi el mismo al de la función show_inventory de la clase inventory
    struct product
    {
        string name;
        int defquantity;
        int quantity;
        int cost;
    }p;
    map<string,product>inv;

    ifstream inventory("inventory.txt");
    string line;
    while(!inventory.eof())
    {
        string id="";
        string name="";
        string defquantity="";
        string quantity="";
        string cost="";
        getline(inventory,line);
        if(line[0]=='\0')
            break;
        int i=0;
        while(line[i]!='.')
        {
            id+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='.')
        {
            name+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='.')
        {
            defquantity+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='.')
        {
            quantity+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='\0')
        {
            cost+=(line[i]);
            i++;
        }
        line.clear();
        int ndefquantity=stoi(defquantity,nullptr,10);
        int nquantity=stoi(quantity,nullptr,10);
        int ncost=stoi(cost,nullptr,10);
        p={name,ndefquantity,nquantity,ncost};
        inv[id]=p;
    }
    inventory.close();
    map<string,product>::iterator it;
    cout<<"\nINVENTARIO\n----------------------------------------\n";
    for(it=inv.begin();it!=inv.end();it++)
    {
        cout<<"ID:     | "<<it->first<<endl;
        cout<<"PRODUCTO| "<<it->second.name<<" x "<<it->second.defquantity<<endl;
        cout<<"CANTIDAD| "<<it->second.quantity<<endl;
        cout<<"COSTO   | "<<it->second.cost<<endl;
        cout<<"----------------------------------------"<<endl;
    }
    inv.clear();
}

int combo::combo_exist(int ID)
{
    /*Se verifica que el combo pedido por el usario exista. retorna 1 si existe, 0 en caso contrario*/
    if(ID<=0 || ID>combolist.size()) return 0;
    else return 1;
}

int combo::transaction(int opt,inventory &inv,float payment)
{
    /*Función que verifica que se pueda realizar la transacción, verificando el pago del usuario y la disponibilidad de los productos con la función extract_quantity de la clase inv (por ello, debe recibir por referencia el objeto inv del main)*/
    int a=1;
    list<com>::iterator it=combolist.begin();
    while(opt>1)
    {
        it++;
        opt--;
    }
    vector<string>ids=it->IDS;
    vector<int>qs=it->Q;
    //vector<string>::iterator iter;

    for(int i=0;i<ids.size();i++)
    {
        cout<<"id: "<<ids[i]<<"  /  qs: "<<qs[i]<<endl;
        if(inv.extract_quantity(ids[i],qs[i])==0)
        {
            a=0;
            break;
        }
    }

    if(a==1)
    {
        if(payment<it->price)
        {
            cout<<"LA CANTIDAD DE DINERO INGRESADA NO ES SUFICIENTE!\n";
            a=0;
        }
        else return_change(it->price,payment);
    }

    return a;
}

void combo::return_change(float price, float payment)
{
    /*Función encargada de procesar el pago, mostrando la menor cantidad de billetes/monedas para devolver el cambio al cliente.*/
    //tomado de la práctica 2
    price=int(price);
    payment=int(payment);
    cout<<"price: "<<price<<endl;
    cout<<"payment: "<<payment<<endl;

    int dinero[10]={0,0,0,0,0,0,0,0,0,0};
    int divisores[10]={50000,20000,10000,5000,2000,1000,500,200,100,50};
    int total=payment-price;
    cout<<"\nSE LE DEVOLVERAN $"<<total<<" pesos";
    cout<<"\nde la siguiente forma: \n";
    for(int i=0;i<10;i++)
    {
        dinero[i]=(total/divisores[i]);
        dinero[i]=dinero[i]-(dinero[i]%1);
        total=(total%divisores[i]);
        cout<<divisores[i]<<": "<<dinero[i]<<endl;
    }
    cout<<"resto: "<<total<<endl<<endl;
}
