/*
PROGRAMA PARA LA GESTIÓN DEL SISTEMA DE VENTA DE ALIMENTOS DEL CINE (PARCIA V1)
POR: SANTIAGO GIRALDO TABARES - CURSO DE INFORMÁTICA II - FACULTAD DE INGENIERÍA - UNIVERSIDAD DE ANTIOQUIA
El siguiente programa está destinado a mostrar las capacidades en el uso de las herramientas del lenguaje c++,
el uso de la prgramación orientada a objetos, la creación de clases y la distribución de tareas entre las mismas,
el acceso a metedos de clases, el uso de contenedores de la librería estandar de plantillas (STL) y la el uso de
memoria dinámica. a la par del uso de cosas menores como uso de ficheros, arreglos, encriptado de información
elaboración de menús iterativos, etc.

El programa permite al administrador hacer lo siguiente:
1. Identificarse en el sistema
2. registrar a otros usuarios
3. Ingresar productos al inventario
4. Ingresar combos para la venta
5. cambiar la contraseña de acceso de administrador
6. borrar el registro de usuarios
7. ver el reporte de las ventas realizadas.
8. visualizar el inventario, el mercado y los combos creados

El programa permite al cliente hacer lo siguiente:
1. Identificarse en el sistema
2. visualizar los combos disponibles
3. comprar un combo
4. Entregar una cantidad deseada de dinero para pagar y que se le muestre el cambio en pantalla con la menos combinación de billetes/monedas
*/


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "combo.h"
#include "inventory.h"


using namespace std;

/*//////////////    P  R  O  T  O  T  I  P  O  S   ( documentación en la implementación )   ////////////////*/

string main_menu();
string admin_menu();
string client_menu();
void client_signup();
int admin_validation();
int client_validation(string cedula, string clave);
int client_exist(string cedula);
void to_ascii(string &binary);
void to_bin(string &ascii);
void crypt(string &binario);
void decrypt(string &binario);
void crypt_admin(string &line);
void decrypt_admin(string &line);
void report(string client, string fila, int sala, int asiento, int combo, float payment);
void show_report();
int is_file_empty(string fname);
int seed = 4;  //semilla de encriptado

/*////////////////////////////////////    M   A   I   N     /////////////////////////////////////////////////*/

int main()
{

    inventory inv;
    combo comb;
    //comb.add_default_combos(); //activar linea de comando para pruebas e introducción de combos por defecto en caso de ausencia del archivo combos.txt
    /* Primero se cargan los archivos donde se almacenan el inventario y los combos guardados */
    inv.load_from_file();
    comb.load_from_file();
    string mainop=main_menu();     //Se muestra el menú principal y se capta la opción introducida por el usuario
    string newpass;                 // variable para guardar una nueva contraseña en caso de entrar al menú de cambio de contraseña

    /*Ingresamos al menú iterativo, el cual se ejecuta siempre y cuando la opcion elegida por el usuario
     * sea diferente de 0 (opcion salir) */

    while(mainop!="0")
    {
        if(mainop=="1")  //Menú del usuario administrador
        {
            if(admin_validation()==1) // Ingresa si el administrador es validado correctamente
            {
                string adminop=admin_menu();   // Abre el menú de administrador y capta la opción elegida por el usuario
                while(adminop!="0")            // y se ejecuta siempre y cuando no seleccione la opción 0 (atras)
                {
                    if(adminop=="1")
                        inv.show_inventory();   //Muestra el inventario (funcion miembro de la clase inventory)
                    if(adminop=="2")
                        inv.add_to_inventory(); //Añade productos al inventario (función miembro de la clase inventory)
                    if(adminop=="3")
                        comb.show_combos();     //Muestra los combos (función miembro de la clase combo)
                    if(adminop=="4")
                    {
                        /*Se introduce un nuevo combo*/
                        string name;
                        string id;
                        vector<string>ids;
                        vector<int>qs;
                        int q;
                        float price;
                        cout<<"\n\nNUEVO COMBO\n"
                              "Ingrese un nombre para el combo: \n";
                        cin.ignore();
                        getline(cin,name);

                        do{                 // Se piden las características del combo siempre que no se introduzca 0 (terminar operación)
                            do{                          //Se pide el ID de los ingredientes a utilizar siempre que estos existan
                            inv.show_inventory();
                            cout<<"\nIngrese ID de ingrediente requerido (0 para dejar de agregar): ";
                            cin>>id;
                            if(inv.product_exist(id)==0)                     //verifica que el producto (id) exista en el inventario (Función miembro de la clase inv)
                                cout<<"\nPRODUCTO NO HALLADO EN EL INVENTARIO\n";
                            }while(inv.product_exist(id)==0);
                            if(id!="0")               //si no se ha cancelado la operación, solicita la cantidad que requiere de dicho ingrediente
                            {
                                ids.push_back(id);
                                do{
                                cout<<"\nIngrese cantidad de ingrediente: ";
                                cin>>q;
                                if(q<=0)
                                    cout<<"\nDEBE INGRESAR UNA CANTIDAD POSITIVA!\n";
                                }while(q<=0);
                                qs.push_back(q);
                            }
                        }while(id!="0");

                        //una vez finalice de agregar ingredientes y sus cantidades, proceda a configurar el precio

                        cout<<"\nIngrese el precio de venta (0 para no agregar combo): ";
                        cin>>price;
                        if(price!=0)
                        {
                            /* Se crea un combo cuyas caracteristicas son las siguientes:
                                un string llamado nombre
                                un vector de strings que almacena los id de los productos que los ingredientes que lo componen
                                un vector de enteros que almacena las cantidad de dichos ingredientes
                                un float que almacena el precio de venta
                                (considerese que los ids y las cantidades se corresponden según sus posiciones en los vectores,
                                  por ejemplo, el ingrediente con id 23 está almacena en la posición 0 del vector ids, y la
                                   cantidad de dicho ingrediente está almacenada en la posición 0 del vector qs)*/

                            comb.create_combo(name,ids,qs,price);
                            inv.write_on_file();  //reescribe el inventario actualizado en el archivo inventory.txt
                        }
                        else cout<<"\nOPERACION CANCELADA\n";   //si el precio ingresado es 0, se toma como cancelar operación y no se añade el combo
                    }
                    if(adminop=="5")
                        show_report(); //se muestra el reporte de ventas
                    if(adminop=="6")
                        client_signup();  //Se proceda a registrar a un nuevo usuario
                    if(adminop=="7")
                    {
                        /*Para borrar el registro se borra el archivo users y se crea uno nuevo*/
                        remove("users");
                        ofstream file("users");
                        cout<<"\n\n\nREGISTRO DE USUARIOS BORRADO CON EXITO\n";
                    }

                    if(adminop=="8")
                    {
                        /*Para cambiar la contraseña se crea un archivo auxiliar donde se almacena la nueva contraseña, se borra
                          el anterior sudo y se renombra a auxiliar como sudo */

                        ofstream w_sudo;
                        w_sudo.open("auxiliar",ios::out);
                        cout<<"\n\n\nINGRESE NUEVA CONTRASE"<<char(165)<<"A DE ADMINISTRADOR: ";
                        cin>>newpass;
                        to_bin(newpass);    //la contraseña debe ser convertida a codigo binario
                        crypt_admin(newpass);   //y a su vez, debe en encriptada
                        w_sudo<<newpass;
                        w_sudo.close();
                        remove("sudo");
                        rename("auxiliar","sudo");
                        cout<<"\n\nCONTRASE"<<char(165)<<"A CAMBIADA\n\n";
                    }
                    adminop=admin_menu(); //se vuelve a mostrar el menu de administrador y se capta la opción que ingrese el usuario
                }
            }
        }
        if(mainop=="2")   //Menú del usuario cliente
        {
            /* Se solicitan los datos de ingreso del usuario, quien debe estar previamente registrado por el
             administrador.*/

            string cedula,clave;
            cout<<"\n__________________________________________";
            cout<<"\n\tUSUARIO CLIENTE:\n\n";
            cout<<"\nIngrese numero de cedula: ";
            cin>>cedula;
            cout<<"\nIngrese su clave: ";
            cin>>clave;

            if(client_validation(cedula,clave)==1) //Si la validación es exitosa...
            {
                string clientop=client_menu();  //Muestra el menú de opciones del cliente
                float payment;                  // variable para almacenar el dinero que ingrese el usario al hacer una compra
                while(clientop!="0")            //El menú se ejecuta siempre que no si ingrese la opcion 0 (atrás)
                {
                    if(clientop=="1")
                        comb.show_combos();     //Muestra los combos disponibles (Función de la clase combo)
                    if(clientop=="2")
                    {
                        //Procede a la compra de un combo

                        int opt;
                        cout<<"\n----------------MENU DE COMPRA----------------\n";
                        do{
                        comb.show_combos();            //Muestra los combos disponibles (Función de la clase combo)
                        cout<<"Ingrese el combo que desea comprar: ";
                        cin>>opt;
                        if(comb.combo_exist(opt)==0)    //Verifica que el combo (opt) ingresado existe en la base de datos de los combos
                            cout<<"\nEL COMBO INGRESADO NO EXISTE!\n";
                        }while(comb.combo_exist(opt)==0);
                        cout<<"\n\nIngrese cantidad con la que va a pagar: ";
                        cin>>payment;
                        if(comb.transaction(opt,inv,payment)==1)    //Si se puede realizar la transacción...
                        {
                            cout<<"\nEL COMBO HA SIDO COMPRADO CON EXITO\n";
                            inv.write_on_file();        //reescriba el inventario habiendo reducido en este las cantidades de los ingredientes requeridos por el combo
                            int sala,asiento;
                            string fila;
                            do{
                            cout<<"\nINGRESE SU SALA (1 a 10): ";
                            cin>>sala;
                            if(sala<0||sala>10)
                                cout<<"\nLa sala ingresada no existe!\n";
                            }while(sala<0 || sala>10);
                            do{
                            cout<<"\nINGRESE FILA DEl ASIENTO (A - M): ";
                            cin>>fila;
                            }while(fila.length()!=1 || (fila[0]<65 || fila[0]>77));
                            do{
                                cout<<"\nINGRESE NUMERO DE ASIENTO (1 - 22): ";
                                cin>>asiento;
                            }while(asiento<1 || asiento>22);
                            cout<<"\n\n----------------------------------------------\n"
                                  "----------------------------------------------\n"
                                  "----------------------------------------------\n"
                                  "G R A C I A S    P O R   S U   C O M P R A :)\n"
                                  "\nSu producto sera entregado en la sala y asiento\n"
                                  "que nos ha indicado\n"
                                  "----------------------------------------------\n"
                                   "----------------------------------------------\n"
                                   "----------------------------------------------\n";
                            report(cedula,fila,sala,asiento,opt,payment);       //Se añade la compra al reporte de ventas
                        }
                        else
                        {
                            cout<<"\nNO SE PUDO CONCRETAR LA COMPRA\n";
                            inv.load_from_file();  //Si no se puede concretar la transacción, cargue el inventario que había almacenado, el cual no ha sufrido los cambios de la operación extract_quantity
                        }
                    }
                    clientop=client_menu(); //Vuelve a mostrar el menú de opciones del usuario y capta la opción ingresada
                }
            }
        }
        mainop=main_menu();     //Vuelve a mostrar el menú principal y capa la opción ingresada
    }
    // sale del main_menu() una vez se ingrese la opcion 0 (salir) y termina el programa
    return 0;
}

/*///////////////////////////////    F  U  N  C  T  I  O  N  S    ///////////////////////////////////////////*/

string main_menu()
{
    /* Menú iterativo. Comprueba que la opción ingresada sea válida por medio de la condición del do_while()
     y dado el caso, muestra un error en pantalla solicitando nuevamente una opcion válida del menú*/

    string option="1";
    do{
        if(option.length()!=1 || (option!="1" && option!="2" && option!="0"))
            cout<<"\nERROR. La opcion ingresada es invalidaaaa!\n";
        cout << "\nSISTEMA DE GESTION - VENTA DE ALIMENTOS DEL CINE\n"
                "------------------------------------------------\n"
                "\t\tMENU PRINCIPAL\n\n"
                "1. Ingresar como administrador\n"
                "2. Ingresar como usuario corriente\n"
                "0. Salir\n\n";
        cin>>option;
    }while(option.length()!=1 || (option!="1" && option!="2" && option!="0"));

    return option;
}

string admin_menu()
{
    /* Menú iterativo para administrador. Comprueba que la opción ingresada sea válida por medio de la condición
     del do_while() y dado el caso, muestra un error en pantalla solicitando nuevamente una opcion válida del menú*/

    string option="1";
    do{
        if(option.length()!=1 || (option!="1" && option!="2" && option!="0"))
            cout<<"\nERROR. La opcion ingresada es invalida!\n";
        cout<<"\n\n\n///MENU DE ADMINISTRADOR///\n\n"
            "Que desea realizar?\n\n"
            "1. Visualizar inventario\n"
            "2. Ingresar productos al inventario\n"
            "3. Visualizar combos de productos\n"
            "4. Ingresar nuevo combo\n"
            "5. Generar reporte de ventas del dia\n"
            "6. Registrar un nuevo usuario cliente\n"
            "7. Borrar usuarios\n"
            "8. Cambiar clave de administrador\n"
            "0. Atras\n\n";
        cin>>option;
    }while(option.length()!=1 || (option!="1" && option!="2" && option!="3" && option!="4" && option!="5" && option!="6" && option!="7" && option!="8" && option!="0"));

    return option;
}

string client_menu()
{
    /* Menú iterativo para clientes. Comprueba que la opción ingresada sea válida por medio de la condición
     del do_while() y dado el caso, muestra un error en pantalla solicitando nuevamente una opcion válida del menú*/

    string option="1";
    do{
        if(option.length()!=1 || (option!="1" && option!="2" && option!="0"))
            cout<<"\nERROR. La opcion ingresada es invalida!\n";
        cout<<"\n\n\n///MENU DE CLIENTE///\n\n"
            "Que desea realizar?\n\n"
            "1. Visualizar combos\n"
            "2. comprar un combo\n"
            "0. Atras\n\n";
        cin>>option;
    }while(option.length()!=1 || (option!="1" && option!="2" && option!="3" && option!="4" && option!="5" && option!="0"));

    return option;
}

void client_signup()
{
    string user,cedula,clave,line,userop;

    do
    {
        do{
            cout<<"\nRESGISTRO DE NUEVO USUARIO\n\n";
            cout<<"Ingrese numero de cedula: ";
            cin>>cedula;
            if(client_exist(cedula)==1)
                cout<<"\nERROR. LA CEDULA INGRESADA YA ESTA REGISTRADA\n\n";
        }while(client_exist(cedula)==1);
        cout<<"\n\nIngrese una clave: ";
        cin>>clave;
        line=cedula+"."+clave;
        to_bin(line);       //traducir a binario linea cedula.clave
        crypt(line);        //encriptar la linea cedula.clave
        ofstream users;
        users.open("users",ios::app);
        users<<line+"\n";   //se ingresa la linea al archivo users
        users.close();
        cout<<"\n\nUSUARIO INGRESADO AL REGISTRO\n\nQue desea hacer?\n1. Registrar nuevo usuario\n0. salir\n\n";
        cin>>userop;

        while((userop[0]!='0' && userop[0]!='1') || userop.length()!=1 )
        {
            cout<<"\n\nDebe ingresar una opcion valida\n\nQue desea hacer?\n1. Registrar nuevo usuario\n0. salir\n\n";
            cin>>userop;
        }
    }while(userop[0]=='1');
}

int admin_validation()
{
    /* Función para validar los datos de ingreso del administrador. Pedirá la clave de ingreso al usuario y la comparará
    con la contraseña escrita y encriptada en el archivo binario "sudo". retorna 1 si se validó correctamente, y 0 en el
    caso contrario*/

    string sudopass,password,userop="1";
    ifstream sudo ("sudo");
    sudo>>sudopass;
    sudo.close();
    decrypt_admin(sudopass);   //desencripta la linea sudopass
    to_ascii(sudopass);        //traduce de binario a ascii la linea sudopass
    cout<<"\n__________________________________________";
    cout<<"\n\tV A L I D A C I O N";
    cout<<"\n\nIngrese clave de administrador:  ";
    cin>>password;
    //Procede a hacer la comparación de la linea sudopass con password
    while(sudopass!=password && userop[0]!='0')
    {
        cout<<"\n\nLa clave ingresada es incorrecta.\n\n1. Intentar nuevamente\n0. salir\n\n";
        cin>>userop;
        while((userop[0]!='1' && userop[0]!='0') || userop.length()!=1)
        {
            cout<<"\nError. debe seleccionar una opcion valida:\n\n1. Intentar nuevamente\n0. salir\n\n";
            cin>>userop;
        }
        if(userop[0]=='1')
        {
            cout<<"\n\nIngrese clave de administrador:  ";
            cin>>password;
        }

    }
    if(userop[0]=='1')
    {
        return 1;
    }
    else
        return 0;
}

int client_validation(string cedula,string clave)
{
    string cedulaclave=cedula+"."+clave;
    int found = 0;
    ifstream users("users");
    while(!users.eof())
    {
        string line="";
        users>>line;
        decrypt(line);
        to_ascii(line);
        if(line==cedulaclave)
        {
            cout<<"\nACCEDISTE COMO CLIENTE\n";
            found=1;
            break;
        }
    }
    if(found==1)
        return 1;
    else
    {
        cout<<"\nACCESO DENEGADO\n";
        return 0;
    }
}

int client_exist(string cedula)
{
    //verificar si en el archivo users se encuentra la cedula ingresada
    int found=1;
    ifstream users;
    users.open("users");
    while(!users.eof())
    {
        string line="";
        found=1;
        int i=0;
        users>>line;    //capta una linea en formato cedula.clave pero en binario y encriptada
        decrypt(line);  //desencripta dicha linea
        to_ascii(line); //traduce de binario a ascii dicha linea
        while(line[i]!='.') //compara cedula con cedula.clave (de esta última comparará los caracteres anteriores al punto (.) )
        {
            if(line[i]!=cedula[i])
            {
                found=0;
                break;
            }
            i++;
        }

        if(found==1)
            break;
    }

    if(found==1)
        return 1;
    else
        return 0;
}

void to_ascii(string &binary)
{
    //traduce de binario a ascii (tomado de la practica 3)
    string ascii;
    int bin=0,rightl=7,leftl=0;
    int j=1;
    int longitud=binary.length();
    while(rightl<longitud)
    {
        for(int i=rightl;i>=leftl;i--)
        {
            if(binary[i]=='1')
                bin+=j;
            j*=2;
        }
        ascii+=char(bin);
        rightl+=8;
        leftl+=8;
        j=1;
        bin=0;
    }
    binary=ascii;
}

void to_bin(string &ascii)
{
    //traduce de ascii a binario (tomado de la practica 3)

    string binary;
    int longitud=ascii.length();
    for(int i=0;i<longitud;i++)
    {
        string byte;
        int residuo,base=1,binario=0;
        while(ascii[i]>0)
        {
            residuo = ascii[i]%2;
            binario = binario + residuo*base;
            ascii[i] = ascii[i]/2;
            base=base*10;
        }
        byte=to_string(binario);
        while(byte.length()!=8)
        {
            byte='0'+byte;
        }
        binary+=byte;
    }
    ascii=binary;
}

void decrypt(string &binario)
{
    //desencripta una cadena encriptada.(tomado de la practica 3)
    for(int i=1;i<seed;i++)
        crypt(binario);  //tan solo encripta otras 'seed-1' veces para obtener la cadena original, ya que este método se basa en el desplazamiento de digitos en conjuntos de 'seed' digitos
}

void crypt(string &binario)
{
    //encripta un string que representa un binario (tomado de la practica 3)
    string binariocopy;
    int length=binario.length();
    int leftl=0,rightl=seed-1;
    while(rightl<length)
    {
        binariocopy+=binario[rightl];
        for(int i=leftl;i<rightl;i++)
        {
            binariocopy+=binario[i];
        }

        leftl+=seed;
        rightl+=seed;
    }
    binario=binariocopy;
}

void decrypt_admin(string &line)
{
    //desencripta un string que representa un binario (metodo solo para contraseña de administrador) (tomado de la practica 3)
    string binariocopy=line;
    int length=line.length();
    int leftl=0,rightl=seed-1;
    int zeroes=0,ones=0;
    for(int i=0;i<seed;i++)
    {
        if(line[i]=='0')
        {
            ones++;
            line[i]='1';
        }
        else
        {
            zeroes++;
            line[i]='0';
        }
    }
    leftl+=seed;
    rightl+=seed;

   while(rightl<length)
    {
        if(zeroes==ones)
        {
            for(int i=leftl;i<=rightl;i++)
            {
                if(line[i]=='0')
                {
                    line[i]='1';
                }
                else
                {
                    line[i]='0';
                }
            }
        }
        else if(zeroes>ones)
            {
                for(int i=(leftl+1);i<=rightl;i+=2)
                {
                    if(line[i]=='0')
                    {
                        line[i]='1';
                    }
                    else
                    {
                        line[i]='0';
                    }
                }
            }
         else
            {
                for(int i=(leftl+2);i<=rightl;i+=3)
                {
                    if(line[i]=='0')
                    {
                        line[i]='1';
                    }
                    else
                    {
                        line[i]='0';
                    }
                }
            }
        zeroes=0;
        ones=0;
        for(int i=leftl;i<=rightl;i++)
        {
            if(line[i]=='0')
                zeroes++;
            else
                ones++;
        }
        leftl+=seed;
        rightl+=seed;
    }
}

void crypt_admin(string &binario)
{
    //encripta una cadena que representa un binario (metodo solo para la contraseña del administrador)(tomado de la practica 3)
    string binariocopy=binario;
    int length=binario.length();
    cout<<"\n\n\n"<<binario;
    int leftl=0,rightl=seed-1;
    int zeroes=0,ones=0;
    for(int i=0;i<seed;i++)
    {
        if(binario[i]=='0')
        {
            zeroes++;
            binario[i]='1';
        }
        else
        {
            ones++;
            binario[i]='0';
        }
    }
    leftl+=seed;
    rightl+=seed;

   while(rightl<length)
    {
        if(zeroes==ones)
        {
            for(int i=leftl;i<=rightl;i++)
            {
                if(binario[i]=='0')
                {
                    binario[i]='1';
                }
                else
                {
                    binario[i]='0';
                }
            }
        }
        else if(zeroes>ones)
            {
                for(int i=(leftl+1);i<=rightl;i+=2)
                {
                    if(binario[i]=='0')
                    {
                        binario[i]='1';
                    }
                    else
                    {
                        binario[i]='0';
                    }
                }
            }
         else
            {
                for(int i=(leftl+2);i<=rightl;i+=3)
                {
                    if(binario[i]=='0')
                    {
                        binario[i]='1';
                    }
                    else
                    {
                        binario[i]='0';
                    }
                }
            }
        zeroes=0;
        ones=0;
        for(int i=leftl;i<=rightl;i++)
        {
            if(binariocopy[i]=='0')
                zeroes++;
            else
                ones++;
        }
        leftl+=seed;
        rightl+=seed;
    }
}

void report(string client, string fila,int sala, int asiento, int combo,float payment)
{
    //accede al archivo report.txt para escritura y para añadir al final del archivo (app).
    ofstream report;
    report.open("report.txt",ios::app);
    report<<"C.C CLIENTE|"<<client+"\n";
    report<<"SALA       |"<<sala<<"\n";
    report<<"ASIENTO:   |"<<fila<<"-"<<asiento<<"\n";
    report<<"COMBO:     |"<<combo<<"\n";
    report<<"PAGO       |"<<payment<<"\n\n";
    //una ves ingresados los datos de la compra en el formato establecido, se cierra el objeto archivo
    report.close();
}

void show_report()
{
    //accede al archivo report.txt para lectura y simplemente imprime en pantalla todas sus lineas
    cout<<"\n\n------------------REPORTE DE VENTAS------------------\n\n";
    string line="";
    ifstream report("report.txt");
    while(!report.eof())
    {
        getline(report,line);
        cout<<line<<endl;
    }
    report.close();
}

int is_file_empty(string fname)
{
    //Verifica que un archivo esté vacío. Función utilizada más que nada para pruebas de depuración y solución de errores intrínsecos del código
    ifstream file(fname);
    string line;
    int i=0;
    while(!file.eof())
    {
        file>>line;
        if(line!="")
        {
            i=1;
            break;
        }
    }
    return i;
}

