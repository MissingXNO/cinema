#include "inventory.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

using namespace std;

inventory::inventory()
{

}

void inventory::add_to_inventory()
{
    /* Función para añadir productos al inventario. Los productos son tomados de un archivo de texto
      llamado market.txt, el cual simula un mercado del cual sacar los productos para añadir en el inventario del cine*/
    ifstream market("market.txt");      //abre market.txt
    string line;
    cout<<"\n\n\nLISTA DEL MERCADO\n\n";
    while(!market.eof())
    {
        //lee linea por linea el archivo y muestra en pantalla todos los productos disponibles en el mercado

        getline(market,line);
        int i=0;
        while(line[i]!='x')
        {
            cout<<line[i];
            i++;
        }
        cout<<" ";
        while(line[i]!='=')
        {
            cout<<line[i];
            i++;
        }
        i++;
        cout<<" por "<<char(36);
        while(line[i]!='\0')
        {
            cout<<line[i];
            i++;
        }
        cout<<endl;
    }
    string option;
    do{
        //Solicita al administrador, de manera iterativa, que añada artículos del mercado al inventario usando su identificador o id
        line="";
        market.close();
        market.open("market.txt");
        cout<<"\nIngrese el numero del producto que desea agregar al inventario ( y 0 para terminar la operacion): ";
        cin>>option;
        while(!market.eof())
        {
            string name;
            string defquantity;
            int quantity;
            string price;
            string num="";
            int i=0;
            getline(market,line);
            while(line[i]!='.')
            {
                num+=line[i];       //hasta antes del '.' será el id del producto
                i++;
            }
            i++;

            //Acorde al formato del archivo market.txt, tomará los productos de la siguiente manera
            if(num==option)
            {
                while(line[i]!='x')
                {
                    name+=line[i];  //antes de la 'x' es el nombre del producto, se almacena caracter a caracter en name
                    i++;
                }
                i++;
                while(line[i]!='=')
                {
                    defquantity+=line[i];   //entre la 'x' y el '=' será la cantidad por defecto del producto
                    i++;
                }
                i++;
                while(line[i]!='\0')
                {
                    price+=line[i];         //entre el = y el caracter nulo será el costo
                    i++;
                }
                cout<<"\nCuantos desea agregar?: ";
                cin>>quantity;              //aquí se almacena la cantidad que desea encargar
                int ndefquantity=stoi(defquantity,nullptr,10);  //se realiza la conversión a entero de defquantity
                int nprice=stoi(price,nullptr,10);      //se realiza la conversión a enter de price
                inventory::p={name,ndefquantity,quantity,nprice*float(quantity),ndefquantity*quantity,nprice*float(quantity)/(ndefquantity*quantity)}; //Se agregan a la estructura p, que representa un producto con sus características
                inv[num]=inventory::p;  //Se agrega la estructura al inventario y se guarda con el id, en un mapa donde la clave es el id y el valor es la estructura
                cout<<"\nSE AGREGARON "<<quantity<<" "<<name<<" x "<<ndefquantity<<endl;
            }
        }
    }while(option!="0");
    write_on_file(); //Se reescribe el inventario actualizado en el archivo
}

void  inventory::show_inventory()
{
    map<string,inventory::product>::iterator it;
    //se recorre el mapa inv y se va imprimiendo cada una de las características de los productos
    cout<<"\nINVENTARIO\n----------------------------------------\n";
    for(it=inventory::inv.begin();it!=inventory::inv.end();it++)
    {
        cout<<"ID:     | "<<it->first<<endl;
        cout<<"PRODUCTO| "<<it->second.name<<" x "<<it->second.defquantity<<endl;
        cout<<"CANTIDAD| "<<it->second.quantity<<endl;
        cout<<"UNIDADES| "<<it->second.neto<<endl;
        cout<<"COSTO   | "<<it->second.cost<<endl;
        cout<<"----------------------------------------"<<endl;
    }
}

void inventory::write_on_file()
{
    /*Se reescribe el inventario actual (el que esté almacenado en memoria durante el tiempo de ejecución
      en el archivo inventory.txt. De esta forma siempre tendremos actualizado el inventario*/
    ofstream inventory("inventory.txt");        //se abre el archivo para escritura en un objeto llamado inventory
    map<string,product>::iterator it;           //se crea un iterador para recorrer el mapa que tenemos como parametro privado en la clase inventory, el cual no es más que el inventario actual
    for(it=inv.begin();it!=inv.end();it++)      //se escriben en el archivo cada una de las parejas que componen el mapa
    {
        inventory<<it->first<<"/"<<it->second.name<<"/"<<it->second.defquantity<<"/"<<it->second.quantity<<"/"<<it->second.cost<<"/"<<it->second.neto<<"/"<<it->second.unitcost<<"\n";
    }
    inventory.close();
}

void inventory::load_from_file()
{
    /*Función que nos permite abrir el archivo inventory.txt y transladar los datos allí escritos en el mapa inv de la clase inventory*/
    ifstream inventory("inventory.txt"); //Se abre para lectura
    string line;
    while(!inventory.eof())
    {
        string id="";
        string name="";
        string defquantity="";
        string quantity="";
        string cost="";
        string neto="";
        string unitcost="";
        getline(inventory,line);
        //el caracter '/' es el separador que el programa utiliza para extraer ordenamente las caracteristicas de los productos en el invetario
        if(line[0]=='\0')
            break;
        int i=0;
        while(line[i]!='/')
        {
            id+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            name+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            defquantity+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            quantity+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            cost+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='/')
        {
            neto+=(line[i]);
            i++;
        }
        i++;
        while(line[i]!='\0')
        {
            unitcost+=(line[i]);
            i++;
        }
        i++;
        line.clear();
        int ndefquantity=stoi(defquantity,nullptr,10);
        int nquantity=stoi(quantity,nullptr,10);
        int ncost=stoi(cost,nullptr,10);
        int nneto=stoi(neto,nullptr,10);
        float nunitcost=stoi(unitcost,nullptr,10);
        inventory::p={name,ndefquantity,nquantity,float(ncost),nneto,nunitcost}; //Guarda los valores extraídos en la estructura p del la clase inventory
        inv[id]=inventory::p;                                               //y añade la estructura como valor al mapa, y al name como la clave
    }
    inventory.close();
}

int inventory::extract_quantity(string ID, int quantity)
{
    /*Función para extraer cantidad de producto del inventario una vez se realice la compra de un combo
        a su vez, disminuye el valor de dicho producto conforme disminuye su cantidad*/
        int i=1; //i es un flag que se retorna igualado a 1 si se pudo realizar la extracción de todos los productos, y se retorna igualada a 0 en el caso contrario
        map<string,product>::const_iterator it = inv.find(ID); //iterador sobre el mapa inv de la clase inventory
        if(it!=inv.end())
        {
            if(inv[ID].neto>=quantity) //siempre que la cantidad a extraer sea menor que la cantidad neta del producto en el inventario...
            {
                inv[ID].neto-=quantity;
                inv[ID].cost-=(inv[ID].unitcost)*quantity;
                if(inv[ID].neto%inv[ID].defquantity==0) //Si al reducir la cantidad neta se acaba un paquete (cantidad por defecto) del producto...
                {
                    /*for(int i=0;i<int(quantity/inv[ID].defquantity);i++)
                        inv[ID].quantity--;*/
                    inv[ID].quantity=inv[ID].neto/inv[ID].defquantity;
                }

            }
            else
            {
                cout<<"\nNO HAY SUFICIENTE CANTIDAD DE "<<inv[ID].name<<" EN EL INVENTARIO.\n";
                i=0;
            }
        }
        else
        {
            cout<<"\nNO HAY PRODUCTOS CON ID "<<ID<<" EN EL INVENTARIO.\n";
            i=0;
        }

        if(inv[ID].cost<inv[ID].unitcost)
            inv[ID].cost=0;
        if(inv[ID].quantity==0)
            inv.erase(ID);
        return i;
}

int inventory::product_exist(string ID)
{
    if(inv.find(ID)==inv.end() && ID!="0") return 0; //no existe
    else return 1;  //existe
}
